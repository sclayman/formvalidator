$(document).ready(function(){

	$.validator.setDefaults({
		errorClass: 'form_error',
		errorElement: 'div',
		errorPlacement: function(error,element) {
			if (element.is("#agree")){
				alert('You must accept the terms and conditions.');
			}
		}
	});

	$("#contactUs").validate({
		rules: {
			fname: {
				required: true,
				minlength: 2
			},
			lname: {
				required: true,
				minlength: 2
			},
			email: {
				required: true,
				email: true
			},
			phone: {
				required: true,
				minlength: 10,
				maxlength: 10,
				number: true
			},
			agree: {
				required: true
			}
		}
	});
		
	$("#contactUs").submit(function() {
		var valid = $("#contactUs").valid();
		if (valid) {
			return true;
		}
		else {
			alert("Please correct the fields in red.");
		}
	});
});